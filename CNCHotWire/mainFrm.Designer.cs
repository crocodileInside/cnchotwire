﻿namespace CNCHotWire
{
    partial class mainFrm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.pXY = new System.Windows.Forms.PictureBox();
            this.pUV = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDXFXYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDXFUVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numWidth = new System.Windows.Forms.NumericUpDown();
            this.numHeight = new System.Windows.Forms.NumericUpDown();
            this.numXYOffsetX = new System.Windows.Forms.NumericUpDown();
            this.numXYOffsetY = new System.Windows.Forms.NumericUpDown();
            this.numUVOffsetX = new System.Windows.Forms.NumericUpDown();
            this.numUVOffsetY = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbHomeCut = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.numHomeY = new System.Windows.Forms.NumericUpDown();
            this.numHomeX = new System.Windows.Forms.NumericUpDown();
            this.btnGcode = new System.Windows.Forms.Button();
            this.numCutSpeed = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.tbStartPosUV = new System.Windows.Forms.TrackBar();
            this.tbStartPosXY = new System.Windows.Forms.TrackBar();
            this.cbCorrection = new System.Windows.Forms.CheckBox();
            this.cbWait = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pXY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pUV)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numXYOffsetX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numXYOffsetY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUVOffsetX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUVOffsetY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHomeY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHomeX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCutSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStartPosUV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStartPosXY)).BeginInit();
            this.SuspendLayout();
            // 
            // pXY
            // 
            this.pXY.Location = new System.Drawing.Point(12, 39);
            this.pXY.Name = "pXY";
            this.pXY.Size = new System.Drawing.Size(300, 300);
            this.pXY.TabIndex = 1;
            this.pXY.TabStop = false;
            // 
            // pUV
            // 
            this.pUV.Location = new System.Drawing.Point(329, 39);
            this.pUV.Name = "pUV";
            this.pUV.Size = new System.Drawing.Size(300, 300);
            this.pUV.TabIndex = 2;
            this.pUV.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(757, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadDXFXYToolStripMenuItem,
            this.loadDXFUVToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadDXFXYToolStripMenuItem
            // 
            this.loadDXFXYToolStripMenuItem.Name = "loadDXFXYToolStripMenuItem";
            this.loadDXFXYToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.loadDXFXYToolStripMenuItem.Text = "Load DXF XY";
            this.loadDXFXYToolStripMenuItem.Click += new System.EventHandler(this.loadDXFXYToolStripMenuItem_Click);
            // 
            // loadDXFUVToolStripMenuItem
            // 
            this.loadDXFUVToolStripMenuItem.Name = "loadDXFUVToolStripMenuItem";
            this.loadDXFUVToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.loadDXFUVToolStripMenuItem.Text = "Load DXF UV";
            this.loadDXFUVToolStripMenuItem.Click += new System.EventHandler(this.loadDXFUVToolStripMenuItem_Click);
            // 
            // numWidth
            // 
            this.numWidth.Location = new System.Drawing.Point(658, 55);
            this.numWidth.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numWidth.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numWidth.Name = "numWidth";
            this.numWidth.Size = new System.Drawing.Size(87, 20);
            this.numWidth.TabIndex = 4;
            this.numWidth.Value = new decimal(new int[] {
            760,
            0,
            0,
            0});
            this.numWidth.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // numHeight
            // 
            this.numHeight.Location = new System.Drawing.Point(658, 94);
            this.numHeight.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numHeight.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numHeight.Name = "numHeight";
            this.numHeight.Size = new System.Drawing.Size(87, 20);
            this.numHeight.TabIndex = 5;
            this.numHeight.Value = new decimal(new int[] {
            590,
            0,
            0,
            0});
            this.numHeight.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // numXYOffsetX
            // 
            this.numXYOffsetX.Location = new System.Drawing.Point(239, 345);
            this.numXYOffsetX.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numXYOffsetX.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numXYOffsetX.Name = "numXYOffsetX";
            this.numXYOffsetX.Size = new System.Drawing.Size(73, 20);
            this.numXYOffsetX.TabIndex = 6;
            this.numXYOffsetX.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // numXYOffsetY
            // 
            this.numXYOffsetY.Location = new System.Drawing.Point(239, 371);
            this.numXYOffsetY.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numXYOffsetY.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numXYOffsetY.Name = "numXYOffsetY";
            this.numXYOffsetY.Size = new System.Drawing.Size(73, 20);
            this.numXYOffsetY.TabIndex = 6;
            this.numXYOffsetY.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // numUVOffsetX
            // 
            this.numUVOffsetX.Location = new System.Drawing.Point(556, 345);
            this.numUVOffsetX.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUVOffsetX.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numUVOffsetX.Name = "numUVOffsetX";
            this.numUVOffsetX.Size = new System.Drawing.Size(73, 20);
            this.numUVOffsetX.TabIndex = 6;
            this.numUVOffsetX.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // numUVOffsetY
            // 
            this.numUVOffsetY.Location = new System.Drawing.Point(556, 371);
            this.numUVOffsetY.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUVOffsetY.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numUVOffsetY.Name = "numUVOffsetY";
            this.numUVOffsetY.Size = new System.Drawing.Size(73, 20);
            this.numUVOffsetY.TabIndex = 6;
            this.numUVOffsetY.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(188, 347);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Offset X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(188, 373);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Offset Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(505, 347);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Offset X";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(505, 373);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Offset Y";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(655, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Machine Width";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(655, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Machine Height";
            // 
            // cbHomeCut
            // 
            this.cbHomeCut.AutoSize = true;
            this.cbHomeCut.Location = new System.Drawing.Point(658, 221);
            this.cbHomeCut.Name = "cbHomeCut";
            this.cbHomeCut.Size = new System.Drawing.Size(73, 17);
            this.cbHomeCut.TabIndex = 13;
            this.cbHomeCut.Text = "Home Cut";
            this.cbHomeCut.UseVisualStyleBackColor = true;
            this.cbHomeCut.CheckedChanged += new System.EventHandler(this.updateInputValues);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(655, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Home Y";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(655, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Home X";
            // 
            // numHomeY
            // 
            this.numHomeY.Location = new System.Drawing.Point(658, 183);
            this.numHomeY.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numHomeY.Name = "numHomeY";
            this.numHomeY.Size = new System.Drawing.Size(87, 20);
            this.numHomeY.TabIndex = 15;
            this.numHomeY.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // numHomeX
            // 
            this.numHomeX.Location = new System.Drawing.Point(658, 144);
            this.numHomeX.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numHomeX.Name = "numHomeX";
            this.numHomeX.Size = new System.Drawing.Size(87, 20);
            this.numHomeX.TabIndex = 14;
            this.numHomeX.Value = new decimal(new int[] {
            380,
            0,
            0,
            0});
            this.numHomeX.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // btnGcode
            // 
            this.btnGcode.Location = new System.Drawing.Point(658, 345);
            this.btnGcode.Name = "btnGcode";
            this.btnGcode.Size = new System.Drawing.Size(87, 46);
            this.btnGcode.TabIndex = 18;
            this.btnGcode.Text = "Generate G-Code";
            this.btnGcode.UseVisualStyleBackColor = true;
            this.btnGcode.Click += new System.EventHandler(this.btnGcode_Click);
            // 
            // numCutSpeed
            // 
            this.numCutSpeed.Location = new System.Drawing.Point(658, 308);
            this.numCutSpeed.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numCutSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCutSpeed.Name = "numCutSpeed";
            this.numCutSpeed.Size = new System.Drawing.Size(87, 20);
            this.numCutSpeed.TabIndex = 15;
            this.numCutSpeed.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numCutSpeed.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(655, 292);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Cut Speed";
            // 
            // tbStartPosUV
            // 
            this.tbStartPosUV.Location = new System.Drawing.Point(329, 345);
            this.tbStartPosUV.Name = "tbStartPosUV";
            this.tbStartPosUV.Size = new System.Drawing.Size(170, 45);
            this.tbStartPosUV.TabIndex = 19;
            this.tbStartPosUV.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // tbStartPosXY
            // 
            this.tbStartPosXY.Location = new System.Drawing.Point(12, 345);
            this.tbStartPosXY.Name = "tbStartPosXY";
            this.tbStartPosXY.Size = new System.Drawing.Size(170, 45);
            this.tbStartPosXY.TabIndex = 20;
            this.tbStartPosXY.ValueChanged += new System.EventHandler(this.updateInputValues);
            // 
            // cbCorrection
            // 
            this.cbCorrection.AutoSize = true;
            this.cbCorrection.Location = new System.Drawing.Point(658, 244);
            this.cbCorrection.Name = "cbCorrection";
            this.cbCorrection.Size = new System.Drawing.Size(97, 17);
            this.cbCorrection.TabIndex = 21;
            this.cbCorrection.Text = "Line Correction";
            this.cbCorrection.UseVisualStyleBackColor = true;
            this.cbCorrection.CheckedChanged += new System.EventHandler(this.updateInputValues);
            // 
            // cbWait
            // 
            this.cbWait.AutoSize = true;
            this.cbWait.Checked = true;
            this.cbWait.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbWait.Location = new System.Drawing.Point(658, 267);
            this.cbWait.Name = "cbWait";
            this.cbWait.Size = new System.Drawing.Size(48, 17);
            this.cbWait.TabIndex = 22;
            this.cbWait.Text = "Wait";
            this.cbWait.UseVisualStyleBackColor = true;
            this.cbWait.CheckedChanged += new System.EventHandler(this.updateInputValues);
            // 
            // mainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 404);
            this.Controls.Add(this.cbWait);
            this.Controls.Add(this.cbCorrection);
            this.Controls.Add(this.tbStartPosXY);
            this.Controls.Add(this.tbStartPosUV);
            this.Controls.Add(this.btnGcode);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numCutSpeed);
            this.Controls.Add(this.numHomeY);
            this.Controls.Add(this.numHomeX);
            this.Controls.Add(this.cbHomeCut);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numUVOffsetY);
            this.Controls.Add(this.numXYOffsetY);
            this.Controls.Add(this.numUVOffsetX);
            this.Controls.Add(this.numXYOffsetX);
            this.Controls.Add(this.numHeight);
            this.Controls.Add(this.numWidth);
            this.Controls.Add(this.pUV);
            this.Controls.Add(this.pXY);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "mainFrm";
            this.Text = "CNCHotWire";
            this.Load += new System.EventHandler(this.mainFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pXY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pUV)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numXYOffsetX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numXYOffsetY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUVOffsetX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUVOffsetY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHomeY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHomeX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCutSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStartPosUV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStartPosXY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pXY;
        private System.Windows.Forms.PictureBox pUV;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDXFXYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDXFUVToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown numWidth;
        private System.Windows.Forms.NumericUpDown numHeight;
        private System.Windows.Forms.NumericUpDown numXYOffsetX;
        private System.Windows.Forms.NumericUpDown numXYOffsetY;
        private System.Windows.Forms.NumericUpDown numUVOffsetX;
        private System.Windows.Forms.NumericUpDown numUVOffsetY;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cbHomeCut;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numHomeY;
        private System.Windows.Forms.NumericUpDown numHomeX;
        private System.Windows.Forms.Button btnGcode;
        private System.Windows.Forms.NumericUpDown numCutSpeed;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TrackBar tbStartPosUV;
        private System.Windows.Forms.TrackBar tbStartPosXY;
        private System.Windows.Forms.CheckBox cbCorrection;
        private System.Windows.Forms.CheckBox cbWait;
    }
}

