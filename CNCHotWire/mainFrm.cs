﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Globalization;
using netDxf;
using netDxf.Entities;
using netDxf.Blocks;
using System.Collections.ObjectModel;

namespace CNCHotWire
{
    public partial class mainFrm : Form
    {
        DxfDocument dxfxy;
        DxfDocument dxfuv;
        Bitmap bmpxy;
        Bitmap bmpuv;
        int previewWidth = 300;
        int previewHeight = 300;
        double mWidth = 1000;
        double mHeight = 500;
        bool doHomeCut = false;
        bool doLineCorrection = false;
        bool doWait = true;
        double homeX = 500;
        double homeY = 100;
        double offsetXY_X = 0;
        double offsetXY_Y = 0;
        double offsetUV_X = 0;
        double offsetUV_Y = 0;
        int startPosXY = 0;
        int startPosUV = 0;
        int startPosXYL = 0;
        int startPosUVL = 0;
        Vector3 transformXY = new Vector3(0, 0, 0);
        Vector3 transformUV = new Vector3(0, 0, 0);
        int cutSpeed = 500;
        double distanceLimit = 1.0;
        string strHeaterOn = "";
        string strHeaterOff = "";

        public mainFrm()
        {
            InitializeComponent();
        }



        private void mainFrm_Load(object sender, EventArgs e)
        {
            updateInputValues(new object(), new EventArgs());
        }


        private void drawImg()
        {
            /*Bitmap bmp = new Bitmap(400, 400);
            Graphics g = Graphics.FromImage(bmp);

            g.Clear(Color.White);

            g.DrawLine(Pens.Blue, 0, 0, 400, 0); // draw top line

            int h = tHeight.Value * 4;
            int w = tWidth.Value * 4;


            g.DrawLine(Pens.DarkGreen, 0, 0, 0, h); // draw height line
            g.DrawLine(Pens.DarkGreen, 0, h, w, h); // draw width line

            g.DrawLine(Pens.Red, 0, 0, w, h);
            g.DrawLine(Pens.Red, 400, 0, w, h);

            label1.Text = "W: " + w;
            label2.Text = "H: " + h;
            label3.Text = "A: " + Math.Sqrt((double)(h * h) + (double)(w * w));
            label4.Text = "B: " + Math.Sqrt((double)(h * h) + (double)((400 - w) * (400 - w)));

            pictureBox1.Image = bmp;*/
        }




        private void loadDXFXYToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "DXF|*.dxf";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                dxfxy = loadDxf(ofd.FileName);
                tbStartPosXY.Value = 0;
            }
            updatePreviews();

        }

        private void loadDXFUVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "DXF|*.dxf";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                dxfuv = loadDxf(ofd.FileName);
                tbStartPosUV.Value = 0;
            }
            updatePreviews();
        }

        private void btnGcode_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "GCode (*.nc)|*.nc|All Files (*.*)|*.*";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                generateGCode(sfd.FileName);
            }
        }


        private DxfDocument loadDxf(string filename)
        {
            DxfDocument dxf;
            try
            {
                dxf = DxfDocument.Load(filename);
                if (dxf != null)
                {

                    Debug.WriteLine("Loaded DXF " + filename);
                    Debug.WriteLine("Points: " + dxf.Points.Count);
                    Debug.WriteLine("Polylines: " + dxf.Polylines.Count);
                    Debug.WriteLine("LwPolylines: " + dxf.LwPolylines.Count);
                    Debug.WriteLine("Splines: " + dxf.Splines.Count);
                    Debug.WriteLine("Lines: " + dxf.Lines.Count);
                    Debug.WriteLine("Arcs: " + dxf.Arcs.Count);
                    Debug.WriteLine("Circles: " + dxf.Circles.Count);

                 

                    foreach (Spline sp in dxf.Splines)
                    {
                        // Possible bug: closed splines render incorrect last point
                        dxf.AddEntity(sp.ToPolyline(100));
                    }

                    foreach (Arc ac in dxf.Arcs)
                    {
                        dxf.AddEntity(ac.ToPolyline(100));
                    }

                    foreach (Polyline pl in dxf.Polylines)
                    {
                        dxf.AddEntity(pl.Explode());
                    }

                    foreach (LwPolyline lwpl in dxf.LwPolylines)
                    {
                        dxf.AddEntity(lwpl.Explode());
                    }

                    foreach (Line ln in dxf.Lines)
                    {
                        //Debug.WriteLine("Line: {0} ; {1}", ln.StartPoint.ToString(), ln.EndPoint.ToString());
                    }


                    Application.DoEvents();
                    updatePreviews();
                }
                else
                {
                    MessageBox.Show("Could not open DXF file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }
            }
            catch (System.NotSupportedException ex)
            {
                MessageBox.Show("Could not open DXF file! \n " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            return dxf;
        }

        private void updatePreviews()
        {
            updatePreviewXY();
            updatePreviewUV();
        }

        private void updatePreviewXY()
        {
            if (bmpxy != null)
                bmpxy.Dispose();
            bmpxy = renderPreview(dxfxy, transformXY, false);
            pXY.Image = bmpxy; 
        }

        private void updatePreviewUV()
        {
            if (bmpuv != null)
                bmpuv.Dispose();
            bmpuv = renderPreview(dxfuv, transformUV, true);
            pUV.Image = bmpuv;
        }



        private Bitmap renderPreview(DxfDocument dxf, Vector3 transform, bool isUV = false)
        {
            Bitmap bmp = new Bitmap(previewWidth, previewHeight);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(Color.Black);

            double scale = (double)previewWidth / mWidth;
            int startPointX = 0;
            int startPointY = 0;

            g.DrawLine(Pens.Blue, 0, (int)(mHeight * scale), previewWidth, (int)(mHeight * scale));

            if (dxf != null)
            {
                List<Vector3> lineDataIn = NormalizeLines(dxf.Lines, transform);
                tbStartPosXY.Maximum = lineDataIn.Count - 1;
                List<Vector3> lineData = new List<Vector3>();
                for(int i=0; i < lineDataIn.Count; i++){
                    if(!(lineDataIn[i].Z > 0)){
                        lineData.Add(lineDataIn[i]);
                    }
                }

                for (int i = 0; i < lineData.Count - 1; i++)
                {
                    g.DrawLine(Pens.Cyan, (int)(lineData[i].X * scale), (int)((mHeight - lineData[i].Y) * scale), (int)(lineData[i + 1].X * scale), (int)((mHeight - lineData[i + 1].Y) * scale));
                }
                g.DrawLine(Pens.Cyan, (int)(lineData[lineData.Count-1].X * scale), (int)((mHeight - lineData[lineData.Count-1].Y) * scale), (int)(lineData[0].X * scale), (int)((mHeight - lineData[0].Y) * scale));


                if (lineData.Count > 0)
                {
                    startPointX = (int)(lineDataIn[startPosXY].X);
                    startPointY = (int)(mHeight - lineDataIn[startPosXY].Y);
                    g.FillEllipse(Brushes.Red, (int)(startPointX*scale) - 2, (int)(startPointY*scale) - 2, 5, 5);

                    if (doHomeCut)
                    {
                        g.DrawLine(Pens.Red, (int)(homeX * scale), (int)((mHeight - homeY) * scale), (int)(homeX * scale), (int)(startPointY*scale));
                        g.DrawLine(Pens.Red, (int)(homeX * scale), (int)(startPointY*scale), (int)(startPointX*scale), (int)(startPointY*scale));

                    }
                }


            }
            /*if (dxf != null)
            {
                foreach (Line ln in dxf.Lines)
                {
                    g.DrawLine(Pens.Cyan, (int)((ofX + ln.StartPoint.X) * scale), (int)((mHeight - ofY - ln.StartPoint.Y) * scale), (int)((ofX + ln.EndPoint.X) * scale), (int)((mHeight - ofY - ln.EndPoint.Y) * scale));
                }
                if (dxf.Lines.Count > 0){
                    startPointX = (int)((ofX + dxf.Lines[startPosXY].StartPoint.X) * scale);
                    startPointY = (int)((mHeight - ofY - dxf.Lines[startPosXY].StartPoint.Y) * scale);
                    g.FillEllipse(Brushes.Red, startPointX-2, startPointY-2, 5, 5);

                    if (doHomeCut)
                    {
                        g.DrawLine(Pens.Red, (int)(homeX * scale), (int)((mHeight - homeY) * scale), (int)(homeX * scale), startPointY);
                        g.DrawLine(Pens.Red, (int)(homeX * scale), startPointY, startPointX, startPointY);
                        
                    }
                }
            }*/

            Font f = new Font("Courier New", 12);
            Font fl = new Font("Arial", 18);

            if(isUV){
                g.DrawString("U", fl, Brushes.White, new PointF(0, 0));
                g.DrawString("V", fl, Brushes.White, new PointF(previewWidth - 25, 0));
            }else{
                g.DrawString("X", fl, Brushes.White, new PointF(0, 0));
                g.DrawString("Y", fl, Brushes.White, new PointF(previewWidth - 25, 0));
            }

            //Brush b = Brushes.Lime;
            /*string str1 = string.Format("X: {0:'+'0000.00;'-'0000.00}", -sx);
            string str2 = string.Format("Y: {0:'+'0000.00;'-'0000.00}", -sy);
            string str3 = string.Format("Zoom: {0:'+'0000.00;'-'0000.00}", zoom);
            g.DrawString("File: " + filename, f, b, new PointF(10, 10));
            g.DrawString(str1, f, b, new PointF(10, 25));
            g.DrawString(str2, f, b, new PointF(10, 40));
            g.DrawString(str3, f, b, new PointF(10, 55));*/

            g.Dispose();

            return bmp;
        }

        private void generateGCode(string outfile)
        {
            StringBuilder sb = new StringBuilder();
            StreamWriter sw = new StreamWriter(outfile, false);

            

           
            Vector3 lastPointXY = new Vector3(0, 0, 0);
            Vector3 lastPointUV = new Vector3(0, 0, 0);

            if (dxfxy != null && dxfuv != null)
            {

           
                //sb.AppendLine(strHeaterOn);
                Vector3 homeLength = transformPointToLength(new Vector3(homeX, homeY, 0));
                sb.AppendLine("G92 " + Vec4ToGPos(homeLength, homeLength, 1000));

                if (dxfxy.Lines.Count == dxfuv.Lines.Count)
                {
                    
                    List<Vector3> lineDataXY = NormalizeLines(dxfxy.Lines, transformXY);
                    List<Vector3> lineDataUV = NormalizeLines(dxfuv.Lines, transformUV);

                    double startPointXY_X = lineDataXY[startPosXY].X;
                    double startPointXY_Y = lineDataXY[startPosXY].Y;
                    double startPointUV_X = lineDataUV[startPosUV].X;
                    double startPointUV_Y = lineDataUV[startPosUV].Y;

                    if (doHomeCut)
                    {
                        sb.AppendLine(Vec4ToGPos(transformPointToLength(new Vector3(homeX, homeY, 0)), transformPointToLength(new Vector3(homeX, homeY, 0)), 500));
                        sb.AppendLine(Vec4ToGPos(transformPointToLength(new Vector3(homeX, startPointXY_Y, 0)), transformPointToLength(new Vector3(homeX, startPointUV_Y, 0)), 500));
                        sb.AppendLine("G4 P2.0");
                        sb.AppendLine(Vec4ToGPos(transformPointToLength(new Vector3(startPointXY_X, startPointXY_Y, 0)), transformPointToLength(new Vector3(startPointUV_X, startPointUV_Y, 0)), 200));
                        sb.AppendLine("G4 P2.0");
                    }

                    for(int i = startPosXY; i < lineDataXY.Count; i++)
                    {
                        if (lineDataXY[i].Z > 0)
                        {
                            if (doWait)
                            {
                                if (lineDataXY[i].Z > 5)
                                {
                                    sb.AppendLine("G4 P2.0");
                                }
                                else
                                {
                                    sb.AppendLine("G4 P0.5");
                                }
                            }
                        }
                        else
                        {

                            Vector3 lxy = lineDataXY[i];
                            Vector3 luv = lineDataUV[i];
                            Vector3 ropeLengthXY = transformPointToLength(lxy);
                            Vector3 ropeLengthUV = transformPointToLength(luv);
                            sb.AppendLine("G1 " + Vec4ToGPos(ropeLengthXY, ropeLengthUV, cutSpeed));
                        }
                    }

                    for (int i = 0; i < startPosXY; i++)
                    {
                        if (lineDataXY[i].Z > 0)
                        {
                            if (doWait)
                            {
                                if (lineDataXY[i].Z > 5)
                                {
                                    sb.AppendLine("G4 P2.0");
                                }
                                else
                                {
                                    sb.AppendLine("G4 P0.5");
                                }
                            }
                        }
                        else
                        {

                            Vector3 lxy = lineDataXY[i];
                            Vector3 luv = lineDataUV[i];
                            Vector3 ropeLengthXY = transformPointToLength(lxy);
                            Vector3 ropeLengthUV = transformPointToLength(luv);
                            sb.AppendLine("G1 " + Vec4ToGPos(ropeLengthXY, ropeLengthUV, cutSpeed));
                        }
                    }


                    if (doHomeCut)
                    {
                        sb.AppendLine(Vec4ToGPos(transformPointToLength(new Vector3(startPointXY_X, startPointXY_Y, 0)), transformPointToLength(new Vector3(startPointUV_X, startPointUV_Y, 0)), cutSpeed));
                        sb.AppendLine("G4 P1.0");
                        sb.AppendLine(Vec4ToGPos(transformPointToLength(new Vector3(homeX, startPointXY_Y, 0)), transformPointToLength(new Vector3(homeX, startPointUV_Y, 0)), 500));
                        sb.AppendLine("G4 P2.0");
                        sb.AppendLine(Vec4ToGPos(transformPointToLength(new Vector3(homeX, homeY, 0)), transformPointToLength(new Vector3(homeX, homeY, 0)), 500)); 
                    }

                }

            }
            else
            {
                MessageBox.Show("Error: Both DXFs must have the same number of Lines");
            }
            sw.Write(sb.ToString());
            sw.Flush();
            sw.Close();
        
        }

       

        private void updateInputValues(object sender, EventArgs e)
        {
            mWidth = (double)numWidth.Value;
            mHeight = (double)numHeight.Value;
            offsetXY_X = (double)numXYOffsetX.Value;
            offsetXY_Y = (double)numXYOffsetY.Value;
            offsetUV_X = (double)numUVOffsetX.Value;
            offsetUV_Y = (double)numUVOffsetY.Value;
            transformXY = new Vector3(offsetXY_X, offsetXY_Y, 0);
            transformUV = new Vector3(offsetUV_X, offsetUV_Y, 0);
            homeX = (double)numHomeX.Value;
            homeY = (double)numHomeY.Value;
            cutSpeed = (int)numCutSpeed.Value;
            doHomeCut = cbHomeCut.Checked;
            doLineCorrection = cbCorrection.Checked;
            doWait = cbWait.Checked;
            startPosXY = tbStartPosXY.Value;
            startPosUV = tbStartPosXY.Value;
            
            updatePreviews();
        }

        private Vector3 transformPointToLength(Vector3 pt)
        {
            //grbl Steps per mm: (360/1.8 * 16) / (Pi * diameter) = 135.812
            //double xhome = Math.Sqrt(Math.Pow(homeY, 2) + Math.Pow(homeX, 2));
            //double yhome = Math.Sqrt(Math.Pow(homeY, 2) + Math.Pow(mWidth - homeX, 2));
            
            double xlen, ylen;

            
            double corr = 1.0;
            double cfactor = -0.000142;
            

            if (doLineCorrection)
            {
                corr = cfactor * Math.Pow(Math.Abs(pt.X - homeX), 2) * (1 - pt.Y/mHeight);
                xlen = Math.Sqrt(Math.Pow(mHeight - (pt.Y + corr), 2) + Math.Pow(pt.X, 2));
                ylen = Math.Sqrt(Math.Pow(mHeight - (pt.Y + corr), 2) + Math.Pow(mWidth - (pt.X), 2));
            }
            else
            {
                xlen = Math.Sqrt(Math.Pow(mHeight - (pt.Y), 2) + Math.Pow(pt.X, 2));
                ylen = Math.Sqrt(Math.Pow(mHeight - (pt.Y), 2) + Math.Pow(mWidth - (pt.X), 2));
            }
            
            return new Vector3(xlen, ylen, 0);
        }

        private string Vec4ToGPos(Vector3 v1, Vector3 v2, int spd)
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = "";
            return "X" + v1.X.ToString("0.0000", nfi) + " Y" + v1.Y.ToString("0.0000", nfi) + " U" + v2.X.ToString("0.0000", nfi) + " V" + v2.Y.ToString("0.0000", nfi) + " F" + spd;
                        
        }

        private List<Vector3> NormalizeLines(ReadOnlyCollection<Line> lines, Vector3 transform)
        {

            List<Vector3> lineData = new List<Vector3>();
            Vector3 lastPoint = transform + lines[0].StartPoint;
            lineData.Add(lastPoint);
            Vector3 lastDelta = new Vector3(0, 0, 0);
            for (int i = 0; i < lines.Count; i++)
            {
                Vector3 curPoint = transform + dxfxy.Lines[i].EndPoint;
                Vector3 delta = curPoint - lastPoint;
                double distance = Vector3.Distance(lastPoint, curPoint);
                if (distance > distanceLimit)
                {
                    int n = (int)Math.Floor(distance / distanceLimit);
                    double m = distance % distanceLimit;
                    Vector3 lineSeg = delta;
                    lineSeg.Normalize();
                    lineSeg = lineSeg * distanceLimit;
                    //Vector3 wait = new Vector3(0, 0, 0);
                    double angle = Vector3.AngleBetween(lastDelta, delta);
                    if (angle > (Math.PI / 6)) //If angle between points is > 30°
                    {
                        //wait = new Vector3(0, 0, 1);
                        lineData.Add(new Vector3(0, 0, 10));
                    }
                    for (int t = 1; t <= n; t++)
                    {
                        lineData.Add(lastPoint + (lineSeg * t));
                    }
                    if (m > 0)
                    {
                        lineData.Add(curPoint);
                    }

                    lastDelta = delta;

                    if (!(lineData[lineData.Count - 1].Z > 0))
                        lineData.Add(new Vector3(0, 0, 1));
                }
                else
                {
                    lineData.Add(curPoint);
                }

                lastPoint = curPoint;
                
            }

            return lineData;
        }

        
        
    



    }
}
